<h1 align = "center"> :fast_forward: RGB Slider :rewind: </h1>

## 🖥 Preview
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117552509_1699567166864294_3274899052420727998_n.jpg?_nc_cat=100&_nc_sid=0debeb&_nc_eui2=AeHszM0Yc7SaKihK6KRK2Tx1-H_WQVxVGj_4f9ZBXFUaP33zjgyidRXp8Xi8qbT42pyRdPj8XFqReY7ilNb2D86u&_nc_ohc=IbwlXRjLNF8AX-yWgG3&_nc_ht=scontent.fbnu2-1.fna&oh=6210419e6a8a0bd4f945a4f68138abf3&oe=5F58B773" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117330812_1699567173530960_2412425416283148155_n.jpg?_nc_cat=111&_nc_sid=0debeb&_nc_eui2=AeF6TsJEacIOudGNpSGwZ_Nae4Lcm7Hrsjt7gtybseuyO0cXxbM2tJ_GDQ5jqq3TmPO6owAousg1EA4qP6ohKNJ4&_nc_ohc=u228atpO3tkAX-zn4Cp&_nc_ht=scontent.fbnu2-1.fna&oh=daeceed337648f48bd2c53ae2c26b50e&oe=5F58505A" width = "500">
</p>

---

## 📖 About
Activity created during the Fullstack bootcamp taught by professor Raphael Gomide by the  Instituto de Gestão e Tecnologia da Informação (IGTI).

With this simple activity, we were able to manipulate the rgb scale through ranges to see its colors.

---

## 🛠 Technologies used
- CSS
- HTML
- Javascript

---

## 🚀 How to execute the project
#### Clone the repository
git clone https://github.com/EPieritz/rgb_slider.git

#### Enter directory
`cd rgb_slider`

#### Run the server
- right click on the `index.html` file
- click on `open with liveserver`

#### That done, open your browser and go to `https://localhost:3000/`

---
Developed with 💙 by Emilyn C. Pieritz
